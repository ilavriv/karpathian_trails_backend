# Generated by Django 2.2.7 on 2019-11-24 17:33

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trails', '0004_trail_track_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='trail',
            name='map_center',
            field=django.contrib.gis.db.models.fields.PointField(null=True, srid=4326),
        ),
    ]
