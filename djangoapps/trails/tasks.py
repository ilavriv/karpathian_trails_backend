from django.core.mail import send_mail
from celery import shared_task


@shared_task
def notify_author(author_email):
    return send_mail(
        'Додане нове рев\'ю',
        'Here is the message.',
        'from@example.com',
        [author_email],
    )
