from django.contrib import admin

from .models import (
    Trail,
    Camp,
    Review
)

# Register your models here.


class CampInline(admin.TabularInline):
    model = Camp


class TrailAdmin(admin.ModelAdmin):

    inlines = (CampInline, )

    list_display = ('name', 'start_from', 'finish',
                    'max_duration', 'min_duration', )

    class Meta:
        model_name = Trail


class ReviewAdmin(admin.ModelAdmin):
    model = Review


admin.site.register(Trail, TrailAdmin)
admin.site.register(Review, ReviewAdmin)
