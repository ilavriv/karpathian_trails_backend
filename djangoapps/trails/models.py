import logging

from django.db import models
from django.db.models import signals
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from django.contrib.gis.db.models import (
    PointField,
    LineStringField
)
from django.conf import settings

from ..common.models import TimestampableModel

from .tasks import (
    notify_author
)

logger = logging.getLogger(__name__)


class Trail(TimestampableModel):
    """
    Trail model
    """
    name = models.CharField(max_length=120, unique=True)
    start_from = models.CharField(max_length=120, verbose_name=_('Start'))
    finish = models.CharField(max_length=120, verbose_name=_('Finish'))
    google_map_url = models.URLField()
    map_center = PointField(null=True)
    short_description = models.CharField(max_length=255)
    description = models.TextField()
    track_path = LineStringField()
    min_duration = models.IntegerField(default=1)
    max_duration = models.IntegerField(default=1)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _('Trail')
        verbose_name_plural = _('Trails')

    def __str__(self):
        return self.name


class Camp(TimestampableModel):
    """
    Trail camp model
    """
    trail = models.ForeignKey('Trail', on_delete=models.CASCADE)
    name = models.CharField(max_length=120)
    coordinates = PointField()

    def __str__(self):
        return self.name


class Review(TimestampableModel):
    """
    Trails review model
    """
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    trail = models.ForeignKey('Trail', on_delete=models.CASCADE)
    content = models.TextField(_('content'))

    def __str__(self):
        return f'{self.trail.name} | {self.content}'


@receiver(signals.post_save, sender=Review)
def review_notify_author(sender, **kwargs):
    review = kwargs.get('instance')

    notify_author.delay(review.author.email)
