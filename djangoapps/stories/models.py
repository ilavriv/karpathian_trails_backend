from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _


from ..common.models import TimestampableModel


class Story(TimestampableModel):
    """
    Trail story model
    """
    title = models.CharField(_('title'), max_length=120)
    text = models.TextField(_('text'))
    published_at = models.DateTimeField(_('published at'))
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    trail = models.ForeignKey('trails.Trail', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Story')
        verbose_name_plural = _('Stories')

    def __str__(self):
        return self.title
