from django.contrib import admin

from .models import (
    Story
)


class StoryAdmin(admin.ModelAdmin):

    class Meta:
        model = Story


admin.site.register(Story, StoryAdmin)
