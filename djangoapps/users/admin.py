from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import (
    User
)

from .forms import (
    AddUserForm,
    UpdateUserForm
)


class CustomUserAdmin(UserAdmin):

    class Meta:
        add_form = AddUserForm
        form = UpdateUserForm
        model = User


admin.site.register(User, CustomUserAdmin)
