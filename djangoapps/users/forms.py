from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import (
    User
)


class AddUserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'saved_trails')


class UpdateUserForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'saved_trails', )
