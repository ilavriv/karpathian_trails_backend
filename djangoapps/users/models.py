from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser

from .managers import UserManager


class User(AbstractUser):
    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = []

    is_email_confirmed = models.BooleanField(
        default=False,
        verbose_name=_('Email confirmed')
    )
    email = models.EmailField(unique=True, verbose_name=_('Email Address'))
    saved_trails = models.ManyToManyField(
        'trails.Trail', related_name='saved_trails')

    objects = UserManager()

    def __str__(self):
        return self.email
