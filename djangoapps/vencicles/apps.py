from django.apps import AppConfig


class VenciclesConfig(AppConfig):
    name = 'vencicles'
