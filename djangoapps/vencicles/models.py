from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.

from ..common.models import TimestampableModel


class Vencicle(TimestampableModel):
    """
    Vencicle model
    """
    start_station = models.CharField(max_length=12e0)
    end_station = models.CharField(max_length=120)

    class Meta:
        verbose_name = _('Vencicle')
        verbose_name_plural = _('Vencicles')

    def __str__(self):
        return f"{self.start_station} - {self.end_station}"
