from rest_framework import serializers

from djangoapps.stories.models import (
    Story
)


class StorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Story
        fields = ('id', 'title', 'text', 'author', 'trail', 'published_at', )
