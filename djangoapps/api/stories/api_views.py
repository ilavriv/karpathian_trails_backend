from django.utils import timezone
from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.response import Response


from djangoapps.stories.models import (
    Story
)

from .serializers import (
    StorySerializer
)


class StoriesViewSet(viewsets.ModelViewSet):
    queryset = Story.objects.filter(published_at__lte=timezone.now()).all()
    serializer_class = StorySerializer

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        payload = {'stories': serializer.data}

        return Response(payload)

    def retrieve(self, request, pk=None):
        story = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(story)
        payload = {'story': serializer.data}

        return Response(payload)
