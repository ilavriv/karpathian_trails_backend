from rest_framework import serializers

from djangoapps.api.common.fields import (
    PointField,
    LineStringField,
)

from djangoapps.trails.models import (
    Trail,
    Camp,
    Review,
)


class CampSerializer(serializers.ModelSerializer):
    coordinates = PointField()

    class Meta:
        model = Camp
        fields = ('id', 'name', 'coordinates', 'created_at', 'updated_at', )


class ReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        fields = ('id', 'content', 'author', 'created_at', 'updated_at', )


class TrailSerializer(serializers.ModelSerializer):
    track_path = LineStringField()
    map_center = PointField()

    class Meta:
        model = Trail
        fields = (
            'id', 'name', 'start_from', 'finish', 'google_map_url',
                  'short_description', 'description', 'min_duration',
                  'max_duration', 'map_center',
                  'author', 'track_path', 'created_at', 'updated_at',
        )
