from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action


from djangoapps.trails.models import (
    Trail,
    Review,
    Camp,
)
from .serializers import (
    TrailSerializer,
    ReviewSerializer,
    CampSerializer,
)


class TrailsViewSet(viewsets.ModelViewSet):
    queryset = Trail.objects.all()
    serializer_class = TrailSerializer

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        payload = {'trails': serializer.data}

        return Response(payload)

    def retrieve(self, request, pk=None):
        trail = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(trail)
        payload = {
            'trail': serializer.data
        }

        return Response(payload)

    @action(detail=True)
    def reviews(self, request, pk=None):
        queryset = Review.objects.filter(trail__pk=pk)
        serializer = ReviewSerializer(queryset, many=True)
        payload = {
            'reviews': serializer.data
        }

        return Response(payload)

    @action(detail=True)
    def camps(self, request, pk=None):
        queryset = Camp.objects.filter(trail__pk=pk)
        serializer = CampSerializer(queryset, many=True)
        payload = {
            'camps': serializer.data
        }

        return Response(payload)
