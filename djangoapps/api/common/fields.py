from django.contrib.gis.geos import Point
from rest_framework import serializers


class PointField(serializers.Field):
    """
    PointField represents gis Point in json for Rest Framework
    """

    COORDINATES_KEYS = ['lon', 'lat']

    def to_representation(self, point):
        coordinates = dict(zip(PointField.COORDINATES_KEYS, point))

        return {
            'type': 'Point',
            'cordinates': coordinates
        }

    def to_internal_value(self, point):
        coordinates = list(point.itervalues())

        return Point(coordinates)


class LineStringField(serializers.Field):

    def to_representation(self, line):
        return []
