from django.urls import path, include

from rest_framework import routers

from .trails.api_views import TrailsViewSet
from .stories.api_views import StoriesViewSet

api_v1_router = routers.DefaultRouter(trailing_slash=True)
api_v1_router.register(r'trails', TrailsViewSet)
api_v1_router.register(r'stories', StoriesViewSet)


urlpatterns = [
    path('', include(api_v1_router.urls)),
]
