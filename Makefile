init-dev:
	pipenv shell

run:
	./manage.py runserver

migrate:
	./manage.py migrate

makemigrations:
	./manage.py makemigrations


celery:
	celery -A config worker -l info


frontend-dev:
	$(MAKE) -C frontend yarn start
